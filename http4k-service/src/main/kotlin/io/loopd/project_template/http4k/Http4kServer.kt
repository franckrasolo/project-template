package io.loopd.project_template.http4k

import io.loopd.project_template.reference_data.api.Currency.Companion.currencyListLens
import io.loopd.project_template.reference_data.api.ReferenceData
import io.loopd.project_template.reference_data.client.ReferenceDataClient
import dev.forkhandles.result4k.recover
import org.http4k.client.OkHttp
import org.http4k.core.Method.GET
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.core.with
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.SunHttp
import org.http4k.server.asServer

fun Http4kApi(referenceData: ReferenceData) = routes(
    "/" bind GET to {
        Response(OK).body("[http4k] Hello World!")
    },
    "/api/currencies/take3" bind GET to {
        Response(OK).with(currencyListLens of referenceData.getCurrencies()
            .recover { emptyList() }
            .take(3)
        )
    }
)

fun main() {
    Http4kApi(ReferenceDataClient(OkHttp())).asServer(SunHttp(8081)).start()
}
