package io.loopd.project_template.http4k

import io.loopd.project_template.reference_data.api.Currency
import io.loopd.project_template.reference_data.api.Currency.Companion.currencyListLens
import io.loopd.project_template.reference_data.api.ReferenceData
import dev.forkhandles.result4k.Success
import io.kotest.assertions.assertSoftly
import io.kotest.matchers.collections.containExactly
import io.kotest.matchers.should
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.core.Status.Companion.OK
import org.http4k.kotest.haveBody
import org.http4k.kotest.haveStatus
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.util.UUID

@DisplayName("Http4k API")
class Http4kApiTests {
    private val referenceData = mockk<ReferenceData>()
    private val underTest = Http4kApi(referenceData)

    @Test
    @DisplayName("""GET / : responds with "Hello World!"""")
    fun `responds with Hello World`() {
        underTest(Request(GET, "/")).let { response ->
            response should haveStatus(OK)
            response should haveBody(".+Hello World!".toRegex())
        }
    }

    @Test
    @DisplayName("GET /api/currencies/take3 : returns the first 3 currencies")
    fun `returns the first 3 currencies`() {
        val currencies = listOf(
            Currency(UUID.randomUUID(), "USD", "United States dollar"),
            Currency(UUID.randomUUID(), "CNY", "Chinese yuan"),
            Currency(UUID.randomUUID(), "JPY", "Japanese yen"),
            Currency(UUID.randomUUID(), "EUR", "Euro"),
            Currency(UUID.randomUUID(), "GBP", "Pound Sterling")
        )

        every { referenceData.getCurrencies() } returns Success(currencies)

        underTest(Request(GET, "/api/currencies/take3")).let { response ->
            assertSoftly {
                response should haveStatus(OK)
                response should haveBody(currencyListLens, containExactly(currencies.take(3)))
            }
        }

        verify(exactly = 1) { referenceData.getCurrencies() }
    }
}
