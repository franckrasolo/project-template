package io.loopd.project_template.reference_data.api

import org.http4k.format.Jackson.auto
import dev.forkhandles.result4k.Result
import org.http4k.core.Body
import java.util.UUID

sealed class ReferenceDataError
object CurrenciesNotFound : ReferenceDataError()

interface ReferenceData {
    fun getCurrencies(): Result<List<Currency>, ReferenceDataError>
}

data class Currency(val id: UUID, val code: String, val name: String) {
    companion object {
        val currencyListLens = Body.auto<List<Currency>>().toLens()
    }
}
