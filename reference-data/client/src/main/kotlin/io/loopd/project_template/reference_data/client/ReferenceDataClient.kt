package io.loopd.project_template.reference_data.client

import io.loopd.project_template.reference_data.api.CurrenciesNotFound
import io.loopd.project_template.reference_data.api.Currency
import io.loopd.project_template.reference_data.api.Currency.Companion.currencyListLens
import io.loopd.project_template.reference_data.api.ReferenceData
import io.loopd.project_template.reference_data.api.ReferenceDataError
import dev.forkhandles.result4k.Failure
import dev.forkhandles.result4k.Result
import dev.forkhandles.result4k.Success
import org.http4k.cloudnative.RemoteRequestFailed
import org.http4k.core.HttpHandler
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.core.Status.Companion.NOT_FOUND
import org.http4k.core.Status.Companion.OK
import org.http4k.core.Uri
import org.http4k.core.then
import org.http4k.filter.ClientFilters
import org.http4k.filter.HandleRemoteRequestFailed

class ReferenceDataClient(rawHttp: HttpHandler) : ReferenceData {
    private val currencyApiUrl = Uri.of("http://localhost:8080/api/currencies")

    // this filter will handle and rethrow non-successful HTTP responses
    private val http = ClientFilters.HandleRemoteRequestFailed({
        status.successful || status == NOT_FOUND || status.serverError
    }).then(rawHttp)

    override fun getCurrencies(): Result<List<Currency>, ReferenceDataError> =
        with(http(Request(GET, currencyApiUrl))) {
            when (status) {
                NOT_FOUND -> Failure(CurrenciesNotFound)
                OK -> Success(currencyListLens(this))
                else -> throw RemoteRequestFailed(
                    status,
                    message = "Failed to retrieve currencies:\n${bodyString()}",
                    uri = currencyApiUrl
                )
            }
        }
}
