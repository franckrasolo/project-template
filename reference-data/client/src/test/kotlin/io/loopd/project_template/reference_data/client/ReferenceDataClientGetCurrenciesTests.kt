package io.loopd.project_template.reference_data.client

import io.loopd.project_template.reference_data.api.CurrenciesNotFound
import io.loopd.project_template.reference_data.api.Currency
import io.loopd.project_template.reference_data.api.Currency.Companion.currencyListLens
import dev.forkhandles.result4k.Failure
import dev.forkhandles.result4k.Success
import io.kotest.assertions.assertSoftly
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.matchers.shouldBe
import io.kotest.matchers.string.shouldContain
import org.http4k.cloudnative.RemoteRequestFailed
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.INTERNAL_SERVER_ERROR
import org.http4k.core.Status.Companion.NOT_FOUND
import org.http4k.core.Status.Companion.OK
import org.http4k.core.with
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import java.util.UUID

@DisplayName("ReferenceDataClient::getCurrencies")
class ReferenceDataClientGetCurrenciesTests {
    private val currencies = listOf(
        Currency(UUID.randomUUID(), "USD", "United States dollar"),
        Currency(UUID.randomUUID(), "GBP", "Pound Sterling"),
        Currency(UUID.randomUUID(), "EUR", "Euro")
    )

    // TODO: test the HTTP request to the Reference Data service

    @Test
    @DisplayName("[happy path] successfully retrieves all currencies from the service")
    fun `retrieves all currencies from the service`() {
        { _: Request -> Response(OK).with(currencyListLens of currencies) }.let { http ->
            ReferenceDataClient(http).getCurrencies() shouldBe Success(currencies)
        }
    }

    @Test
    @DisplayName("[sad path] fails with 'CurrenciesNotFound' when the HTTP status is NOT_FOUND")
    fun `returns an empty list when receiving a NOT_FOUND`() {
        { _: Request -> Response(NOT_FOUND) }.let { http ->
            ReferenceDataClient(http).getCurrencies() shouldBe Failure(CurrenciesNotFound)
        }
    }

    @Test
    @DisplayName("[sad path] throws an exception for any other HTTP status")
    fun `throws an exception for any other HTTP status`() {
        { _: Request -> Response(INTERNAL_SERVER_ERROR).body(RuntimeException("a server bug").stackTraceToString()) }
            .let { http ->
                shouldThrow<RemoteRequestFailed> {
                    ReferenceDataClient(http).getCurrencies()
                }.also { exception ->
                    assertSoftly {
                        exception.status shouldBe INTERNAL_SERVER_ERROR
                        exception.message shouldContain "Failed to retrieve currencies:".toRegex()
                        exception.message shouldContain ".+a server bug".toRegex()
                    }
                }
            }
    }
}
