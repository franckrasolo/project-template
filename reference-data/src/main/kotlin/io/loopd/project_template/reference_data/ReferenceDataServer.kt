package io.loopd.project_template.reference_data

import io.loopd.project_template.reference_data.api.Currency
import io.loopd.project_template.reference_data.api.Currency.Companion.currencyListLens
import org.http4k.core.HttpHandler
import org.http4k.core.Method.GET
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.core.with
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.SunHttp
import org.http4k.server.asServer
import java.util.UUID

val currencies = listOf(
    Currency(UUID.fromString("6ee4ce13-d7af-44cb-8e58-3511e0cb2ac6"), "USD", "United States dollar"),
    Currency(UUID.fromString("0e2a37f3-b061-4cf1-a81f-aa74c3843c6c"), "CNY", "Chinese yuan"),
    Currency(UUID.fromString("55ee8a8c-52e4-4286-8bcf-15efbfd2fa35"), "JPY", "Japanese yen"),
    Currency(UUID.fromString("545f2c1d-182d-4072-9bf6-9724002f2888"), "EUR", "Euro"),
    Currency(UUID.fromString("62222354-2d95-487e-abef-4a66298d1cb7"), "GBP", "Pound sterling")
)
fun ReferenceDataApi(): HttpHandler = routes(
    "/api/currencies" bind GET to { Response(OK).with(currencyListLens of currencies) }
)

fun main() {
    ReferenceDataApi().asServer(SunHttp(8080)).start()
}
