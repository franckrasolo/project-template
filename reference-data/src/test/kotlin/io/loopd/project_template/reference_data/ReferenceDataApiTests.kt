package io.loopd.project_template.reference_data

import io.loopd.project_template.reference_data.api.Currency.Companion.currencyListLens
import io.kotest.matchers.be
import io.kotest.matchers.should
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.core.Status.Companion.OK
import org.http4k.kotest.haveBody
import org.http4k.kotest.haveStatus
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("ReferenceData API")
class ReferenceDataApiTests {
    private val app = ReferenceDataApi()

    @Test
    @DisplayName("GET /api/currencies : returns a list of currencies")
    fun `returns a list of currencies`() {
        app(Request(GET, "/api/currencies")).let { response ->
            response should haveStatus(OK)
            response should haveBody(currencyListLens, be(currencies))
        }
    }
}
