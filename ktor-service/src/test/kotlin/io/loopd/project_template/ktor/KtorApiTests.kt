package io.loopd.project_template.ktor

import io.kotest.assertions.ktor.shouldHaveContent
import io.kotest.assertions.ktor.shouldHaveStatus
import io.kotest.matchers.string.shouldContain
import io.ktor.application.Application
import io.ktor.http.HttpMethod.Companion.Get
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test

@DisplayName("Ktor API")
class KtorApiTests {

    @Test
    @DisplayName("""GET / : responds with "Hello World!"""")
    fun `responds with Hello World`() {
        withTestApplication(Application::module) {
            with(handleRequest(Get, "/")) {
                response shouldHaveStatus OK
                response.content shouldContain ".+Hello World!".toRegex()
                response shouldHaveContent "[ktor] Hello World!"
            }
        }
    }
}
