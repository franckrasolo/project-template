package io.loopd.project_template.ktor

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.http.ContentType.Text.Plain
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.jetty.EngineMain

@Suppress("unused") // referenced in application.conf
@JvmOverloads
fun Application.module(@Suppress("UNUSED_PARAMETER") testing: Boolean = false) {
    routing {
        get("/") {
            call.respondText("[ktor] Hello World!", contentType = Plain)
        }
    }
}

fun main(args: Array<String>) = EngineMain.main(args)
