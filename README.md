# PROJECT TEMPLATE

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=franckrasolo_project-template&metric=alert_status)](https://sonarcloud.io/dashboard?id=franckrasolo_project-template)
[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=franckrasolo_project-template&metric=security_rating)](https://sonarcloud.io/dashboard?id=franckrasolo_project-template)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=franckrasolo_project-template&metric=vulnerabilities)](https://sonarcloud.io/dashboard?id=franckrasolo_project-template)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=franckrasolo_project-template&metric=coverage)](https://sonarcloud.io/dashboard?id=franckrasolo_project-template)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=franckrasolo_project-template&metric=code_smells)](https://sonarcloud.io/dashboard?id=franckrasolo_project-template)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=franckrasolo_project-template&metric=duplicated_lines_density)](https://sonarcloud.io/dashboard?id=franckrasolo_project-template)
[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=franckrasolo_project-template&metric=ncloc)](https://sonarcloud.io/dashboard?id=franckrasolo_project-template)

---

A trivial Gradle multi-module project that allows the comparison between http4k and Ktor from the perspective of practicing _**test-driven development**_ in Kotlin.

---

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-white.svg)](https://sonarcloud.io/dashboard?id=franckrasolo_project-template)

This project uses the [SonarScanner for Gradle](https://redirect.sonarsource.com/doc/gradle.html) to trigger the analysis. Continuous Integration is configured to build and analyse all branches and pull requests.
