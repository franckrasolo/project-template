#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o pipefail
set -o nounset

if [[ "$#" -ne 1 || ! -f "$1" ]]; then
    echo ">> Prints the code coverage percentage for a given JaCoCo CSV report"
    echo "Usage: $0 [file]"
    echo "where"
    echo "  file is the path to a JaCoCo coverage report in CSV format"
    exit 1
fi

awk -F ',' -f <(cat - <<- 'EOF'
    {
        instructions += $4 + $5;
        covered += $5
    } END {
        print "Coverage:", 100*covered/instructions, "%"
    }
EOF
) $1
