SHELL := /bin/bash -o errexit

ROOT_DIR := $(shell dirname $(realpath $(firstword $(MAKEFILE_LIST))))
GRADLE ?= $(ROOT_DIR)/gradlew
GRADLE_OPTS := "-Dorg.gradle.jvmargs='-Xms512m -Xmx512m -XX:+HeapDumpOnOutOfMemoryError'"

.PHONY: targets clean compile tests check-coverage ci

targets:
	@echo "Available targets:"
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null \
		| awk -v RS= -F: '/^# File/,/^# Finished Make data base/ { if ($$1 !~ "^[#.]") { print ">>",$$1 } }' \
		| sort

clean:
	$(GRADLE) clean

compile:
ifdef dir
	$(GRADLE) --project-dir $(dir) --rerun-tasks testClasses
else
	$(GRADLE) testClasses
endif

tests:
ifdef dir
	$(GRADLE) --project-dir $(dir) --rerun-tasks test
else
	$(GRADLE) --rerun-tasks test
endif

check-coverage:
ifdef dir
	$(GRADLE) --project-dir $(dir) jacocoTestCoverageVerification
else
	$(GRADLE) jacocoRootCoverageVerification
endif

ci:
	$(GRADLE) clean --rerun-tasks test jacocoRootCoverageVerification
